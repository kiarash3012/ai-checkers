class Checkers:
    def __init__(self):
        # possible numbers in board B: -2 -1, WS: 0, W: 1 2
        self.board = {1: {1: 0, 2: 1, 3: 0, 4: 1, 5: 0, 6: 1, 7: 0, 8: 1},
                      2: {1: 1, 2: 0, 3: 1, 4: 0, 5: 1, 6: 0, 7: 1, 8: 0},
                      3: {1: 0, 2: 1, 3: 0, 4: 1, 5: 0, 6: 1, 7: 0, 8: 1},
                      4: {1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0},
                      5: {1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0},
                      6: {1: -1, 2: 0, 3: -1, 4: 0, 5: -1, 6: 0, 7: -1, 8: 0},
                      7: {1: 0, 2: -1, 3: 0, 4: -1, 5: 0, 6: -1, 7: 0, 8: -1},
                      8: {1: -1, 2: 0, 3: -1, 4: 0, 5: -1, 6: 0, 7: -1, 8: 0}}

        self.turn = -1  # 1 or -1
        self.ai_vs_ai = True

    def make_move(self, move, army):
        if self.board[move[0]][move[1]] == 0:
            self.board[move[0]][move[1]] = self.board[army[0]][army[1]]
            self.board[army[0]][army[1]] = 0

        # kill black
        elif self.board[move[0]][move[1]] == -1 or self.board[move[0]][move[1]] == -2 or\
                self.board[move[0]][move[1]] == 2 or self.board[move[0]][move[1]] == 1:
            # left
            if move[1] < army[1]:
                # up
                if move[0] < army[0]:
                    self.board[move[0]][move[1]] = 0
                    self.board[army[0] - 2][army[1] - 2] = self.board[army[0]][army[1]]
                    self.board[army[0]][army[1]] = 0
                # down
                else:
                    self.board[move[0]][move[1]] = 0
                    self.board[army[0] + 2][army[1] - 2] = self.board[army[0]][army[1]]
                    self.board[army[0]][army[1]] = 0
            # right
            else:
                # up
                if move[0] < army[0]:
                    self.board[move[0]][move[1]] = 0
                    self.board[army[0] - 2][army[1] + 2] = self.board[army[0]][army[1]]
                    self.board[army[0]][army[1]] = 0
                # down
                else:
                    self.board[move[0]][move[1]] = 0
                    self.board[army[0] + 2][army[1] + 2] = self.board[army[0]][army[1]]
                    self.board[army[0]][army[1]] = 0

        for a in range(1, 9):
            for b in range(1, 9):
                self.turn_to_king(a, b)

        # change_turn
        if self.turn == 1:
            self.turn = -1
        else:
            self.turn = 1

    def possible_moves(self, x, y):
        def is_enemy(color, i, j):
            if (i < 1) or (i > 8) or (j < 1) or (j > 8):
                return False
            if color == 1:
                if self.board[i][j] == -2 or self.board[i][j] == -1:
                    return True
                return False
            if color == -1:
                if self.board[i][j] == 2 or self.board[i][j] == 1:
                    return True
                return False
        
        def is_killable(x, y, i, j):
            if (i < 1) or (i > 8) or (j < 1) or (j > 8):
                return False
            # left
            if j < y:
                # up
                if i < x:
                    if (x - 2 > 0) and (y - 2 > 0):
                        if self.board[x - 2][y - 2] == 0:
                            return True
                # down
                else:
                    if (x + 2 < 9) and (y - 2 > 0):
                        if self.board[x + 2][y - 2] == 0:
                            return True
            # right
            else:
                # up
                if i < x:
                    if (x - 2 > 0) and (y + 2 < 9):
                        if self.board[x - 2][y + 2] == 0:
                            return True
                # down
                else:
                    if (x + 2 < 9) and (y + 2 < 9):
                        if self.board[x + 2][y + 2] == 0:
                            return True
            return False
        
        list_of_moves = list()
        list_of_kills = list()
        # kings
        if self.board[x][y] == -2 or self.board[x][y] == 2:
            # white
            if self.board[x][y] == 2:
                if (y > 1) and (y < 8):
                    if self.board[x - 1][y + 1] == 0 and y + 1 < 9:
                        list_of_moves.append([x - 1, y + 1])
                    elif is_enemy(1, x-1, y+1):
                        if is_killable(x, y, x - 1, y + 1):
                            list_of_kills.append([x - 1, y + 1])
                    if self.board[x - 1][y - 1] == 0 and y - 1 > 0:
                        list_of_moves.append([x - 1, y - 1])
                    elif is_enemy(1, x - 1, y - 1):
                        if is_killable(x, y, x - 1, y - 1):
                            list_of_kills.append([x - 1, y - 1])
                    if y + 1 < 9 and x + 1 < 9:
                        if self.board[x + 1][y + 1] == 0:
                            list_of_moves.append([x + 1, y + 1])
                    elif is_enemy(1, x + 1, y + 1):
                        if is_killable(x, y, x + 1, y + 1):
                            list_of_kills.append([x + 1, y + 1])
                    if x + 1 < 9 and self.board[x + 1][y - 1] == 0 and y - 1 > 0:
                        list_of_moves.append([x + 1, y - 1])
                    elif is_enemy(1, x + 1, y - 1):
                        if is_killable(x, y, x + 1, y - 1):
                            list_of_kills.append([x + 1, y - 1])
                else:
                    if y == 1:
                        if self.board[x + 1][y + 1] == 0 and x + 1 < 9:
                            list_of_moves.append([x + 1, y + 1])
                        elif is_enemy(1, x + 1, y + 1):
                            if is_killable(x, y, x + 1, y + 1):
                                list_of_kills.append([x + 1, y + 1])
                        if x - 1 > 0:
                            if self.board[x - 1][y + 1] == 0:
                                list_of_moves.append([x - 1, y + 1])
                            elif is_enemy(1, x - 1, y + 1):
                                if is_killable(x, y, x - 1, y + 1):
                                    list_of_kills.append([x - 1, y + 1])
                    if y == 8:
                        if self.board[x + 1][y - 1] == 0 and x + 1 < 9:
                            list_of_moves.append([x - 1, y - 1])
                        elif is_enemy(1, x-1, y-1):
                            if is_killable(x, y, x - 1, y - 1):
                                list_of_kills.append([x - 1, y - 1])
                        if y - 1 > 0:
                            if self.board[x + 1][y - 1] == 0:
                                list_of_moves.append([x + 1, y - 1])
                            elif is_enemy(1, x+1, y-1):
                                if is_killable(x, y, x + 1, y - 1):
                                    list_of_kills.append([x + 1, y - 1])
            # black
            if self.board[x][y] == -2:
                if (x > 1) and (x < 8):
                    if y + 1 < 9:
                        if self.board[x - 1][y + 1] == 0:
                            list_of_moves.append([x - 1, y + 1])
                        elif is_enemy(-1, x-1, y+1):
                            if is_killable(x, y, x - 1, y + 1):
                                list_of_kills.append([x-1, y+1])
                    if y - 1 > 0:
                        if self.board[x - 1][y - 1] == 0:
                            list_of_moves.append([x - 1, y - 1])
                        elif is_enemy(-1, x - 1, y - 1):
                            if is_killable(x, y, x - 1, y - 1):
                                list_of_kills.append([x - 1, y - 1])
                    if y + 1 < 9:
                        if self.board[x + 1][y + 1] == 0:
                            list_of_moves.append([x + 1, y + 1])
                        elif is_enemy(-1, x + 1, y + 1):
                            if is_killable(x, y, x + 1, y + 1):
                                list_of_kills.append([x + 1, y + 1])
                    if y - 1 > 0:
                        if self.board[x + 1][y - 1] == 0:
                            list_of_moves.append([x + 1, y - 1])
                        elif is_enemy(-1, x + 1, y - 1):
                            if is_killable(x, y, x + 1, y - 1):
                                list_of_kills.append([x + 1, y - 1])
                else:
                    if x == 1:
                        if x + 1 < 9 and y + 1 < 9 and self.board[x + 1][y + 1] == 0 :
                            list_of_moves.append([x + 1, y + 1])
                        elif is_enemy(-1, x + 1, y + 1):
                            if is_killable(x, y, x + 1, y + 1):
                                list_of_kills.append([x + 1][y + 1])
                        if y - 1 > 0:
                            if self.board[x + 1][y - 1] == 0:
                                list_of_moves.append([x + 1, y - 1])
                            elif is_enemy(-1, x + 1, y - 1):
                                if is_killable(x, y, x + 1, y - 1):
                                    list_of_kills.append([x + 1, y - 1])
                    if x == 8:
                        if y + 1 < 9:
                            if self.board[x - 1][y + 1] == 0:
                                list_of_moves.append([x - 1, y + 1])
                            elif is_enemy(-1, x - 1, y + 1):
                                if is_killable(x, y, x - 1, y + 1):
                                    list_of_kills.append([x - 1, y + 1])
                        if y - 1 > 0:
                            if x + 1 < 9 and self.board[x + 1][y - 1] == 0:
                                list_of_moves.append([x + 1, y - 1])
                            elif is_enemy(-1, x + 1, y - 1):
                                if is_killable(x, y, x + 1, y - 1):
                                    list_of_kills.append([x + 1, y - 1])

        # non kings
        else:
            # white
            if self.board[x][y] == 1:
                if x + 1 < 9:
                    if (y > 1) and (y < 8):
                        if self.board[x + 1][y + 1] == 0:
                            list_of_moves.append([x + 1, y + 1])
                        elif is_enemy(1, x + 1, y + 1):
                            if is_killable(x, y, x + 1, y + 1):
                                list_of_kills.append([x + 1, y + 1])
                        if self.board[x + 1][y - 1] == 0:
                            list_of_moves.append([x + 1, y - 1])
                        elif is_enemy(1, x + 1, y - 1):
                            if is_killable(x, y, x + 1, y - 1):
                                list_of_kills.append([x + 1, y - 1])
                    else:
                        if y == 1:
                            if self.board[x + 1][y + 1] == 0:
                                list_of_moves.append([x + 1, y + 1])
                            elif is_enemy(1, x + 1, y + 1):
                                if is_killable(x, y, x + 1, y + 1):
                                    list_of_kills.append([x + 1, y + 1])
                        if y == 8:
                            if self.board[x + 1][y - 1] == 0:
                                list_of_moves.append([x + 1, y - 1])
                            elif is_enemy(1, x + 1, y - 1):
                                if is_killable(x, y, x + 1, y - 1):
                                    list_of_kills.append([x + 1, y - 1])
            # black
            if self.board[x][y] == -1:
                if x - 1 > 0:
                    if (y > 1) and (y < 8):
                        if self.board[x - 1][y - 1] == 0:
                            list_of_moves.append([x - 1, y - 1])
                        elif is_enemy(-1, x - 1, y - 1):
                            if is_killable(x, y, x - 1, y - 1):
                                list_of_kills.append([x - 1, y - 1])
                        if self.board[x - 1][y + 1] == 0:
                            list_of_moves.append([x - 1, y + 1])
                        elif is_enemy(-1, x - 1, y + 1):
                            if is_killable(x, y, x - 1, y + 1):
                                list_of_kills.append([x - 1, y + 1])

                    else:
                        if y == 1:
                            if self.board[x - 1][y + 1] == 0:
                                list_of_moves.append([x - 1, y + 1])
                            elif is_enemy(-1, x - 1, y + 1):
                                if is_killable(x, y, x - 1, y + 1):
                                    list_of_kills.append([x - 1, y + 1])
                        if y == 8:
                            if self.board[x - 1][y - 1] == 0:
                                list_of_moves.append([x - 1, y - 1])
                            elif is_enemy(-1, x - 1, y - 1):
                                if is_killable(x, y, x - 1, y - 1):
                                    list_of_kills.append([x - 1, y - 1])

        return list_of_moves, list_of_kills

    def h(self, x, y):
        moves, kill_moves = self.possible_moves(x, y)

        # todo calculate real h
        black_soldiers, black_kings = self.find_army(-1)
        white_soldiers, white_kings = self.find_army(1)

        # static param
        h0 = 5
        # black soldiers params
        w1 = 1
        h1 = len(black_soldiers)
        # white soldiers params
        w2 = 1
        h2 = len(white_soldiers)
        # black kings params
        w3 = 1
        h3 = len(black_kings)
        # white kings params
        w4 = 1
        h4 = len(white_kings)
        # threatened white army params
        w5 = 1
        h5 = 0
        for soldier in black_soldiers:
            _temp, list_of_kills = self.possible_moves(soldier[0], soldier[1])
            h5 += len(list_of_kills)
        for king in black_kings:
            _temp, list_of_kills = self.possible_moves(king[0], king[1])
            h5 += len(list_of_kills)
        # threatened black army params
        w6 = 1
        h6 = 0
        for soldier in white_soldiers:
            _temp, list_of_kills = self.possible_moves(soldier[0], soldier[1])
            h6 += len(list_of_kills)
        for king in white_kings:
            _temp, list_of_kills = self.possible_moves(king[0], king[1])
            h6 += len(list_of_kills)

        h = h0 + w1 * h1 + w2 * h2 + w3 * h3 + w4 * h4 + w5 * h5 + w6 * h6

        if len(kill_moves) > 0:
            return kill_moves[0], h+1
        elif len(moves) > 0:
            return moves[0], h
        return False, False

    def find_army(self, color):
        list_of_soldiers = list()
        list_of_kings = list()
        if color == 1:
            for i in range(1, 9):
                for j in range(1, 9):
                    if self.board[i][j] == 1:
                        list_of_soldiers.append([i, j])
                    if self.board[i][j] == 2:
                        list_of_kings.append([i, j])
        if color == -1:
            for i in range(1, 9):
                for j in range(1, 9):
                    if self.board[i][j] == -1:
                        list_of_soldiers.append([i, j])
                    if self.board[i][j] == -2:
                        list_of_kings.append([i, j])
        return list_of_soldiers, list_of_kings

    def any_one_wins(self, next_move):
        solders, kings = self.find_army(self.turn)
        if len(solders) + len(kings) == 0:
            win_statement = 'white has won the game' if self.turn == 1 else 'black has won the game'
            print(win_statement)
            return False
        if len(next_move) == 0:
            _white_solders, _white_kings = self.find_army(1)
            white_army = len(_white_kings) + len(_white_solders)
            _dark_solders, _dark_kings = self.find_army(-1)
            dark_army = len(_dark_kings) + len(_dark_solders)

            if dark_army > white_army:
                win_statement = 'black has won the game'
            else:
                win_statement = 'white has won the game'
            print(win_statement)
            return False
        return True

    def run(self):
        if self.ai_vs_ai:
            keep_playing = True
            count = 1
            while keep_playing:
                if count == 137:
                    print('army_to_move')
                move, army_to_move = self.next_move(self.turn)

                keep_playing = self.any_one_wins(move)
                print(move)
                self.make_move(move, army_to_move)
                self.paint_board()
                print('move #{}'.format(count))
                count += 1
                if count > 200:
                    keep_playing = False
                    print('no more moves game ended')

        else:
            pass

    def next_move(self, turn):
        soldiers, kings = self.find_army(turn)

        army_to_move = None
        best_option = []
        best_option_h = 0

        for king in kings:
            _temp_option, _temp_h = self.h(king[0], king[1])
            if _temp_h > best_option_h:
                best_option = _temp_option
                army_to_move = king
                best_option_h = _temp_h
        for soldier in soldiers:
            _temp_option, _temp_h = self.h(soldier[0], soldier[1])
            if _temp_option is False:
                continue
            if _temp_h > best_option_h:
                best_option = _temp_option
                army_to_move = soldier
                best_option_h = _temp_h
        return best_option, army_to_move

    # todo make this more efficient by combining it to only the moved piece on the board
    def turn_to_king(self, x, y):
        color = self.board[x][y]
        if color not in [2, -2]:
            if color == 1 and x == 8:
                self.board[x][y] = 2
            if color == -1 and x == 1:
                self.board[x][y] = -2

    def paint_board(self):
        board_picture = ''
        print('  |  1 |  2 |  3 |  4 |  5 |  6 |  7 |  8 |')
        print('  -----------------------------------------')
        count = 1
        for i in self.board:
            board_picture += '{} |'.format(count)
            count += 1
            for j in self.board[i]:
                board_picture += ' %2s |' % self.board[i][j]
            board_picture += '\n'
        print(board_picture)


checkers = Checkers()
print('let the game begins ai vs ai now !!!')
checkers.run()
